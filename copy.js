(function () {
  if (typeof normalizeString === "undefined") {
    window.normalizeString = (string) =>
      string
        .replace(/～/g, "〜")
        .replace(/\r/g, "")
        .replace(/\n/g, "")
        .replace(/\[/g, "「")
        .replace(/\]/g, "」")
        .replace(/―/g, "ー")
        .trim();
  }

  if (typeof getNormalizedCellText === "undefined") {
    window.getNormalizedCellText = (cell) =>
      normalizeString(cell.innerText || "");
  }

  if (typeof createRowText === "undefined") {
    window.createRowText = (furigana, kanji, vietnamese) =>
      `${furigana}${kanji ? ` / ${kanji}` : ""}\t${vietnamese}`;
  }

  if (typeof copyVocab === "undefined") {
    window.copyVocab = function () {
      const table = document.getElementsByTagName("table")[0];
      if (!table) return;

      const rows = table.getElementsByTagName("tr");
      const normalizedRows = [];

      for (let index = 0; index < rows.length; index++) {
        if (index === 0) continue;

        const row = rows[index];
        const cells = row.getElementsByTagName("td");
        if (cells.length === 1) continue;

        const furigana = getNormalizedCellText(cells[0]);

        if (cells.length === 2) {
          const vietnamese = getNormalizedCellText(cells[1]);
          const nextCell = rows[index + 1].getElementsByTagName("td")[0];
          const kanji = getNormalizedCellText(nextCell);
          normalizedRows.push(createRowText(furigana, kanji, vietnamese));
        }

        if (cells.length === 3) {
          const kanji = getNormalizedCellText(cells[1]);
          const vietnamese = getNormalizedCellText(cells[2]);
          normalizedRows.push(createRowText(furigana, kanji, vietnamese));
        }
      }

      chrome.runtime.sendMessage({
        type: "copy",
        text: normalizedRows.join("\r\n"),
      });
    };
  }

  copyVocab();
})();
