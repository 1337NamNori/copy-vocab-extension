# DungMori to Quizlet Vocabulary Importer

This is a Chrome extension that helps you import vocabulary from DungMori to Quizlet. It does this by copying the vocabulary to your clipboard, which you can then paste into Quizlet.

## Features

- Copy vocabulary from DungMori to your clipboard with one click
- Delete all cards from your Quizlet set with one click

## How to Use

1. Navigate to a DungMori vocabulary page.
2. Click on the extension icon in the Chrome toolbar.
3. Click on the "Copy Vocabulary" button to copy the vocabulary to your clipboard.
4. Navigate to your Quizlet set and paste the vocabulary.

## Keyboard Shortcuts

- `MacCtrl+Shift+C` (Mac) or `Ctrl+Shift+C` (other platforms): Copy vocabulary to clipboard
- `MacCtrl+Shift+D` (Mac) or `Ctrl+Shift+D` (other platforms): Delete all cards from Quizlet set

## Development

This project is developed using JavaScript. To contribute to the project, you can clone the repository and make changes to the `popup.js` and `copy.js` files.

## License

This project is licensed under the MIT License.
