(function deleteCard() {
  const buttons = document.querySelectorAll("[aria-label='Delete this card']");

  for (const button of buttons) {
    button.click();
  }
})();
