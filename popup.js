const copyButton = document.getElementById("copy");
const deleteButton = document.getElementById("delete");

const getActiveTab = () => new Promise(resolve => {
  chrome.tabs.query({ active: true, currentWindow: true }, tabs => resolve(tabs[0]));
});

const executeScript = (tabId, file) => {
  chrome.scripting.executeScript({
    target: { tabId },
    files: [file],
  })
  .then(() => console.log("Injected the content script."))
  .catch(err => console.error(err));
};

const handleButtonClick = (button, file) => {
  if (button.disabled) return;
  getActiveTab().then(tab => executeScript(tab.id, file));
};

copyButton.addEventListener("click", () => handleButtonClick(copyButton, "copy.js"));
deleteButton.addEventListener("click", () => handleButtonClick(deleteButton, "delete.js"));

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.type === "copy") {
    navigator.clipboard.writeText(request.text)
      .then(() => {
        const hasStatus = document.getElementById("status");
        if (hasStatus) hasStatus.remove();
        const textElement = document.createElement("p");
        textElement.id = "status";
        textElement.textContent = `Text copied to clipboard. ${request.text.split("\r\n").length} vocabulary words found.`;
        document.getElementById("copy").insertAdjacentElement("afterend", textElement);
      })
      .catch(err => console.error("Could not copy text: ", err));
  }
});

chrome.commands.onCommand.addListener(command => {
  if (command === "copy_vocab") handleButtonClick(copyButton, "copy.js");
  else if (command === "delete_all") handleButtonClick(deleteButton, "delete.js");
});

getActiveTab().then(tab => {
  const url = tab.url;
  const dungmoriRegex = /^https:\/\/dungmori\.com\/.*tu-vung(?!.*flash-card)/;
  const quizletRegex = /^https:\/\/quizlet\.com\/.*edit$/;
  copyButton.disabled = !dungmoriRegex.test(url);
  deleteButton.disabled = !quizletRegex.test(url);
});
